<?php
namespace dgwht;


class File {
	private $FILE;
	public $Msg;
	
	function __construct($config=[], $type='Tx') {
//		$config = [
//			"appid" => "",
//			"appkey" => "",
//			"bucket" => "",
//			"region" => "",
//		];
		$type = strtolower($type);
		if($type=='tx') $this->FILE = new \dgwht\file\Cos($config);
		if($type=='ali') $this->FILE = new \dgwht\file\Oss($config);
	}
	
	public function up( $key, $srcPath, $headers=[] ) {
		if(!isset($key) || !isset($srcPath)){
			$this->Msg = "本地路径或远程路径不能为空。";
			return false;
		}
		$ret = $this->FILE->up( $key, $srcPath, $headers );
		$this->Msg = $this->FILE->Msg;
		if(!$ret){
			return false;
		}
		return true;
	}
	
	public function getSignUrl($time=10, $key=null){
		return $this->FILE->getSignUrl($time, $key);
	}
	
	public function del($key=null){
		if($key==null) return false;
		return $this->FILE->del($key);
	}
}